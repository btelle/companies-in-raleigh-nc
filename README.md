# companies-in-raleigh-nc

A list of tech-related companies in the Raleigh/Durham/Chapel Hill, NC area.

| Company Name | Location | Industry | Glassdoor | Careers Page |
|--------------|----------| :------: | :-------: | :----------: |
| [IBM](https://www.ibm.com/) | RTP, NC | Technology | [Glassdoor](https://www.glassdoor.com/Reviews/IBM-Reviews-E354.htm) | [Careers](https://careers.ibm.com/ListJobs/All/?lang=en) |
| [Cisco Systems](https://www.cisco.com/) | RTP, NC | Technology | [Glassdoor](https://www.glassdoor.com/Reviews/Cisco-Systems-Reviews-E1425.htm) | [Careers](https://jobs.cisco.com/jobs/SearchJobs/) |
| [Lenovo](https://www.lenovo.com/) | RTP, NC | Technology | [Glassdoor](https://www.glassdoor.com/Reviews/Lenovo-Reviews-E8034.htm) | [Careers](https://www.lenovocareers.com/) |
| [Dell EMC](https://www.dellemc.com/en-us/index.htm) | RTP, NC | Technology | [Glassdoor](https://www.glassdoor.com/Reviews/Dell-Technologies-Reviews-E1327.htm) | [Careers](https://jobs.delltechnologies.com/category/dell-emc-jobs/5787/60140/1) |
| [SAS Institute](https://www.sas.com/en_us/home.html) | Cary, NC | Software | [Glassdoor](https://www.glassdoor.com/Reviews/SAS-Institute-Reviews-E3807.htm) | [Careers](https://globalcareers-sas.icims.com/jobs/intro) |
| [NetApp](https://www.netapp.com/) | RTP, NC | Technology | [Glassdoor](https://www.glassdoor.com/Reviews/NetApp-Reviews-E5406.htm) | [Careers](https://jobs.netapp.com/) |
| [Red Hat](https://www.redhat.com/) | Raleigh, NC | Technology | [Glassdoor](https://www.glassdoor.com/Reviews/Red-Hat-Reviews-E8868.htm) | [Careers](https://www.redhat.com/en/jobs) |
| [Microsoft](https://www.microsoft.com/en-us/) | Morrisville, NC | Technology | [Glassdoor](https://www.glassdoor.com/Reviews/Microsoft-Reviews-E1651.htm) | [Careers](https://careers.microsoft.com/us/en/search-results) |
| [Citrix](https://www.citrix.com/) | Raleigh, NC | Technology | [Glassdoor](https://www.glassdoor.com/Reviews/Citrix-Reviews-E5432.htm) | [Careers](https://jobs.citrix.com/) |
| [Dude Solutions](https://www.dudesolutions.com/) | Cary, NC | Technology | [Glassdoor](https://www.glassdoor.com/Reviews/Dude-Solutions-Reviews-E879994.htm) | [Careers](https://recruiting.ultipro.com/DUD1000DUDE/JobBoard/a1929d18-68c4-4ba7-b6ec-7d47c69143bb/?q=&o=postedDateDesc&w=&wc=&we=&wpst=) |
| [Fidelity Investments](https://www.fidelity.com/) | Durham, NC | Finance | [Glassdoor](https://www.glassdoor.com/Reviews/Fidelity-Investments-Reviews-E2786.htm) | [Careers](https://jobs.fidelity.com/) |
| [Credit Suisse](https://www.credit-suisse.com/) | Raleigh, NC | Finance | [Glassdoor](https://www.glassdoor.com/Reviews/Credit-Suisse-Reviews-E3141.htm) | [Careers](https://www.credit-suisse.com/careers/en/search-and-apply.html) |
| [First Citizens Bank](https://www.firstcitizens.com/) | Raleigh, NC | Finance | [Glassdoor](https://www.glassdoor.com/Reviews/First-Citizens-Bank-Reviews-E1404.htm) | [Careers](https://jobs.firstcitizens.com/) |
| [Duke University](https://www.duke.edu/) | Durham, NC | University | [Glassdoor](https://www.glassdoor.com/Reviews/Duke-University-Reviews-E2775.htm) | [Careers](https://careers.duke.edu/) |
| [University of North Carolina](https://www.unc.edu/) | Chapel Hill, NC | University | [Glassdoor](https://www.glassdoor.com/Reviews/UNC-Chapel-Hill-Reviews-E561649.htm) | [Careers](https://hr.unc.edu/careers/) |
| [North Carolina State University](https://www.ncsu.edu/) | Raleigh, NC | University | [Glassdoor](https://www.glassdoor.com/Reviews/North-Carolina-State-University-Reviews-E23944.htm) | [Careers](https://jobs.ncsu.edu/) |
| [Epic Games](https://www.epicgames.com/) | Cary, NC | Gaming | [Glassdoor](https://www.glassdoor.com/Reviews/Epic-Games-Reviews-E266904.htm) | [Careers](https://epicgames.wd5.myworkdayjobs.com/en-US/Epic_Games) |
| [Lulu.com](https://www.lulu.com/) | Morrisville, NC | Publishing | [Glassdoor](https://www.glassdoor.com/Reviews/Lulu-com-Reviews-E237327.htm) | [Careers](https://jobs-lulu.icims.com/jobs/search) |
| [PrecisionHawk](https://www.precisionhawk.com/) | Raleigh, NC | Drones | [Glassdoor](https://www.glassdoor.com/Reviews/PrecisionHawk-Reviews-E1273893.htm) | [Careers](https://www.precisionhawk.com/careers) |
| [Pendo](https://www.pendo.io/) | Raleigh, NC | Technology | [Glassdoor](https://www.glassdoor.com/Reviews/Pendo-Reviews-E1061465.htm) | [Careers](https://www.pendo.io/careers/) |
| [DropSource](https://www.dropsource.com/) | Raleigh, NC | Mobile Apps | [Glassdoor](https://www.glassdoor.com/Reviews/Dropsource-Reviews-E955287.htm) | [Careers](https://www.linkedin.com/company/queue-software/jobs/)? |
| [Constellation Digital Partners](https://www.constellation.coop/) | Raleigh, NC | Finance | Unavailable | [Careers](https://constellation-digital-partners.breezy.hr/) |
| [Reveal Mobile](https://revealmobile.com/) | Raleigh, NC | Technology | [Glassdoor](https://www.glassdoor.com/Reviews/Reveal-Mobile-Reviews-E1405817.htm) | [Careers](https://revealmobile.com/careers/) |
| [Pryon](https://www.pryon.com/) | Raleigh, NC | AI | [Glassdoor](https://www.glassdoor.com/Reviews/Pryon-Reviews-E3082733.htm) | [Careers](https://www.pryon.com/careers/) |
| [Diveplane](https://diveplane.com/) | Raleigh, NC | Industry | Unavailable | [Careers](https://diveplane.com/careers/) |
